from flask import Flask,abort,jsonify,request,render_template
import pandas as pd
import numpy as np
import pickle
from fasttext.FastText import load_model, tokenize
model = load_model('/home/chandan/anaconda3/lib/python3.7/site-packages/gensim/test/test_data/lee_fasttext_new.bin')

my_model=pickle.load(open('/home/chandan/nepali_news_aggregator/nepali-news-clustering/nlp/clusters.pkl','rb'))

app=Flask(__name__)

@app.route('/',methods=['GET','POST'])
def make_predict():
    if request.method=='POST':
        data=request.form.get("text")
        # convert our json to numpy array
        test1 = np.array([ np.mean(np.array([ model.get_word_vector(w) for w in article ] ), axis = 0 ) for article in data] )

        # np array goes into clusters and predict out
        y_hat=my_model.predict(test1.reshape(-1,10))
        output=y_hat[0]
        return render_template('index.html',cluster=output)
    else:
        return render_template('index.html',cluster='')


if __name__=="__main__":
    app.run(debug=True)


    